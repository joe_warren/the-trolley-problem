function loadHourglass(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET","hourglass.svg",true);
    xhr.overrideMimeType("image/svg+xml");
    var handler = function(){
        var hourglass = this.responseXML.documentElement;
        hourglass.id = "hourglass";
        document.getElementById("hourglassContainer")
            .appendChild(hourglass);
        hourglass.addEventListener("click", function(){
            hourglass.classList.add("animated");
        });
    }
    xhr.onload = handler
    xhr.send();
}

function redoAnimation(){
    var hourglass = document.getElementById("hourglass")
    hourglass.classList.remove("animated");
    window.setTimeout(function(){
        startAnimation();
    }, 10);
}

function resetAnimation(){
    var hourglass = document.getElementById("hourglass")
    hourglass.classList.remove("animated");
    hourglass.classList.add("spinning");
    window.setTimeout(function(){
        hourglass.classList.remove("spinning");
        startAnimation();
    }, 500);
}

function startAnimation(){
    document.getElementById("hourglass").classList.add("animated");
}
