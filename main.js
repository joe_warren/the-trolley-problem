function isDag(tree){
    function recurse(node, visited){
        if(visited.indexOf(node) !== -1){
            return false
        }
        function check(type){
            if(tree[node][type]){
                return recurse(tree[node][type], visited.concat(node))
            }
            return true;
        }
        return check("active") && check("passive");
    }
    return recurse("start", []);
}
function loadTree(treeCB){
    var request = new XMLHttpRequest();
    request.open('GET', "tree.json");
    request.responseType = 'json';
    request.send();
    request.onload = function(){
        treeCB(request.response);
    };
}


function startGame(){
    loadTree(function(tree){
        window.console.log(isDag(tree)?"the graph is a DAG":"error, not a DAG");

        startAnimation();
        
        function endGame(){
            var textNode = document.getElementById("text");
            textNode.innerHTML = "game over";
        }

        function runNode(name){
            var textNode = document.getElementById("text");
            textNode.innerHTML = tree[name].text;
            var button = document.getElementById("button");
            document.getElementById("button");
            button.innerHTML = tree[name].action;
            function runNext(next) {
                if (next){
                    runNode(next);
                } else {
                    endGame();
                }
            };
            var passiveTimer = window.setTimeout(function(){
                redoAnimation();
                runNext(tree[name].passive);
            }, 7500);
            button.onclick = function(){
                resetAnimation();
                window.clearTimeout(passiveTimer);
                runNext(tree[name].active);
            };
        };  
        runNode("start");
    });
}

window.onload = function(){
    loadHourglass();
}

